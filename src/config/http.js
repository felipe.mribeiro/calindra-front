import axios from 'axios'

const localUrlApi = `http://localhost:3000`

const http = axios.create({
    baseURL: Process.env.REACT_APP_API || localUrlApi
})

http.defaults.headers['content-type'] = 'aplication/json'

export default http