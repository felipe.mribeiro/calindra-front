import React from 'react'

import { Route, Router, Switch } from 'react-router-dom'
import { Home } from './views'

import history from './config/history'

const Routers = () => (
    <Router history={history}>
        <Switch>
            <Route exact component={Home} path='/' />
        </Switch>
    </Router>
)

export default Routers